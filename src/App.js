import logo from "./logo.svg";
import "./App.css";
import InputForm from "./Components/InputForm";
import TableList from "./Components/TableList";

function App() {
  return (
    <div >
      <InputForm/>
    </div>
  );
}

export default App;

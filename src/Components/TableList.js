import React, { Component } from "react";
import Swal from "sweetalert2";

export default class TableList extends Component {
  handleAlert = (item) => {
    console.log(item);
    Swal.fire({
      html:
        `ID : ${item.id} <br>` +
        ` Email : ${item.emailSet} <br>` +
        ` Username : ${item.userNameSet} <br>` +
        ` Age : ${item.ageSet}`,
    });
  };

  render() {
    return (
      <div>
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
          <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-blue-100 dark:bg-gray-700 dark:text-gray-400">
              <tr className="text-center py-5">
                <th scope="col" class="px-6 py-5">
                  ID
                </th>
                <th scope="col" class="px-6 py-5">
                  EMAIL
                </th>
                <th scope="col" class="px-6 py-5">
                  USERNAME
                </th>
                <th scope="col" class="px-6 py-5">
                  AGE
                </th>
                <th scope="col" class="px-6 py-5">
                  ACTION
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.data.map((item) => (
                <tr class="text-center text-lg text-black-900 even:bg-pink-100 odd:bg-gray-100">
                  <td class="px-6 py-4"> {item.id} </td>
                  <td class="px-6 py-4">
                    {" "}
                    {item.emailSet == "" ? "Null" : item.emailSet}{" "}
                  </td>
                  <td class="px-6 py-4">
                    {" "}
                    {item.userNameSet == "" ? "Null" : item.userNameSet}{" "}
                  </td>
                  <td class="px-6 py-4">
                    {" "}
                    {item.ageSet == "" ? "Null" : item.ageSet}{" "}
                  </td>
                  <td class="px-6 py-4">
                    <button
                      onClick={() => this.props.changeAction(item.id)}
                      type="button"
                      class={`py-2 px-5 mr-2 mb-2 text-sm font-medium text-white bg-blue-700 rounded-lg border 
                      ${
                        item.stutas == "pending"
                          ? " bg-red-600"
                          : "bg-green-700"
                      } `}
                    >
                      {item.stutas}
                    </button>
                    <button
                      onClick={() => {
                        this.handleAlert(item);
                      }}
                      type="button"
                      class="py-2 px-5 mr-2 mb-2 text-sm font-medium text-white bg-blue-700 rounded-lg border "
                    >
                      Show more
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

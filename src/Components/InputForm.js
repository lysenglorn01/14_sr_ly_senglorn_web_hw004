import React, { Component } from "react";
import TableList from "./TableList";

export default class InputForm extends Component {
  constructor() {
    super();
    this.state = {
      stuInfor: [
        // value will store in here.
      ],

      id: null,
      email: "",
      userName: "",
      age: "",
      stutas: "pendding"
    };
  }

  changeEmail = (e) => {
    this.setState(
      {
        email: e.target.value,
      }
    )
  };

  changeUserName = (e) => {
    console.log(this.changeUserName)
    this.setState(
      {
        userName: e.target.value,
      }
    );
  };

  changeAge = (e) => {
    this.setState(
      {
        age: e.target.value,
      }
    );
  };

  changeAction = (newId) => {
    this.state.stuInfor.map((item) => 
      item.id == newId ? (item.stutas = item.stutas == "pendding" ? "done" : "pendding") : item.id
    );
    this.setState(
      {
        stuInfor: this.state.stuInfor
      }
    )
  }

  onSubmit = () => {
    const newObj = {
      id: this.state.stuInfor.length + 1, 
      emailSet: this.state.email, 
      userNameSet: this.state.userName, 
      ageSet: this.state.age,
      stutasSet: this.state.stutas
    };

    this.setState({
      stuInfor: [...this.state.stuInfor, newObj], newStu: ""
    }, console.log(this.state.stuInfor));
  };

  render() {
    return (
      <div class="bg-green-200 grid place-items-center min-h-screen overflow-hidden">
        <div className="mt-[30px] text-4xl font-bold">
          <span className="bg-clip-text text-transparent bg-gradient-to-r from-purple-500 to-pink-500">
            Please fill your{" "}
          </span>
          <span>information</span>
        </div>

        <div className="w-[70%]">
          {/* Your Email */}
          <label
            for="email"
            class="block mb-2 text-xl font-medium text-gray-900 dark:text-white"
          >
            Your Email
          </label>
          <div class="relative mb-6">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-gray-500 dark:text-gray-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
                <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
              </svg>
            </div>
            <input
              type="email"
              id="email"
              class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-4  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="lysenglorn01@gamil.com"
              onChange={this.changeEmail}
            />
          </div>

          {/* userName */}
          <div className="mb-6">
            <label
              for="userName"
              class="block mb-2 text-xl font-medium text-gray-900 dark:text-white"
            >
              userName
            </label>
            <div class="flex">
              <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
                @
              </span>
              <input
                type="text"
                id="surname"
                class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-4  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Ly Senglorn"
                onChange={this.changeUserName}
              />
            </div>
          </div>

          {/* Age */}
          <div>
            <label
              for="age"
              class="block mb-2 text-xl font-medium text-gray-900 dark:text-white"
            >
              Age
            </label>
            <div class="flex">
              <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
                ❤
              </span>
              <input
                type="text"
                id="age"
                class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-4  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="22"
                onChange={this.changeAge}
              />
            </div>
          </div>

          {/* button submit */}
          <div className="text-center my-2 py-10">
            <button
              onClick={this.onSubmit}
              type="submit"
              class=" text-black border-2 border-blue-600 bg-white hover:bg-blue-300 hover:text-white  font-medium rounded-lg text-base w-[25%] px-5 py-2 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Register
            </button>
          </div>

          {/* import component Tabel File */}
          <div className="mb-10">
            <TableList data ={this.state.stuInfor} />
          </div>
        </div>
      </div>
    );
  }
}
